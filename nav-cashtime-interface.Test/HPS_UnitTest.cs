﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Neowell.NavInvoiceInterface.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nav_cashtime_interface.Test
{
    [TestClass]
    public class HPS_UnitTest
    {
        [TestMethod]
        public void SendAnInvoice()
        {
            QueueInvoiceResultEnum res;
            using (Entities.Cashtime2009_testEntities context = new Entities.Cashtime2009_testEntities())
            {
                var InvoiceID = context.Invoice.Where(x => x.InvNo != null &&
                    (!x.IsNAVExportSuccessful.HasValue ||
                        (x.IsNAVExportSuccessful.HasValue && !x.IsNAVExportSuccessful.Value))
                 ).Select(x => x.InvoiceID).First();

                nav_cashtime_interface.infrastructure.INavInvoiceSender isender = new nav_cashtime_interface.HPS.HPSInvoiceSender();

                nav_cashtime_interface.infrastructure.InvoiceSender sender = new infrastructure.InvoiceSender(isender);

                res = sender.SendAnInvoiceToNav(InvoiceID);

            }

            Assert.AreEqual(res, QueueInvoiceResultEnum.Queued);
        }

        [TestMethod]
        public void GenerateAll_HPS_Invoices()
        {
            int succes = 0;
            int max = 0;

            using (Entities.Cashtime2009_testEntities context = new Entities.Cashtime2009_testEntities())
            {
                var invoices = context.Invoice.Where(x => x.InvNo != null 
                        &&
                        !String.IsNullOrEmpty(x.TextSellerTaxNum)
                        &&
                    (!x.IsNAVExportSuccessful.HasValue 
                    ||(x.IsNAVExportSuccessful.HasValue && !x.IsNAVExportSuccessful.Value)
                    )
                    &&
                    x.InvDate.Value.Year>=2018
                 ).Select(x => x.InvoiceID).ToList();

                max = invoices.Count;

                nav_cashtime_interface.infrastructure.INavInvoiceSender isender = new nav_cashtime_interface.HPS.HPSInvoiceSender();

                nav_cashtime_interface.infrastructure.InvoiceSender sender = new infrastructure.InvoiceSender(isender);

                object lockobj = new object();
                invoices.ToList().AsParallel<int>().ForAll(delegate (int InvoiceID)
                {
                    QueueInvoiceResultEnum res = sender.SendAnInvoiceToNav(InvoiceID);

                    if (res == QueueInvoiceResultEnum.Queued)
                    {
                        lock (lockobj)
                        {
                            succes += 1;
                        }
                    }
                });

            }

            Assert.AreEqual(max, succes);
        }
    }
}
