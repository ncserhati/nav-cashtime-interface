﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Neowell.NavInvoiceInterface.Domain.Enums;
using System.Linq;

namespace nav_cashtime_interface.Test
{
    [TestClass]
    public class SZKT_UnitTest
    {
        [TestMethod]
        public void GenerateAFailed_SZKT_Invoice()
        {
            using (Entities.SZKT_CashTime_TestEntities context = new Entities.SZKT_CashTime_TestEntities())
            {
                var invoice = context.Invoice.FirstOrDefault(x => x.IsNAVExportSuccessful.HasValue && !x.IsNAVExportSuccessful.Value);

                nav_cashtime_interface.infrastructure.INavInvoiceSender isender = new nav_cashtime_interface.SZKT.SZKTInvoiceSender();

                nav_cashtime_interface.infrastructure.InvoiceSender sender = new infrastructure.InvoiceSender(isender);

                QueueInvoiceResultEnum res = sender.SendAnInvoiceToNav(invoice.InvoiceID);

                Assert.AreEqual(res, QueueInvoiceResultEnum.Queued);
            }
        }

        [TestMethod]
        public void GenerateAll_SZKT_Invoices()
        {
            int succes = 0;
            int max = 0;

            using (Entities.SZKT_CashTime_TestEntities context = new Entities.SZKT_CashTime_TestEntities())
            {
                var invoices = context.Invoice.Where(x=>x.InvNo != null && 
                    (!x.IsNAVExportSuccessful.HasValue || 
                        (x.IsNAVExportSuccessful.HasValue && !x.IsNAVExportSuccessful.Value))
                        &&
                        x.InvDate.HasValue && x.InvDate.Value.Year>=2016
                 ).Select(x=> x.InvoiceID).ToList();

                max = invoices.Count;

                nav_cashtime_interface.infrastructure.INavInvoiceSender isender = new nav_cashtime_interface.SZKT.SZKTInvoiceSender();

                nav_cashtime_interface.infrastructure.InvoiceSender sender = new infrastructure.InvoiceSender(isender);

                object lockobj = new object();
                invoices.ToList().AsParallel<int>().ForAll(delegate (int InvoiceID)
                {
                    QueueInvoiceResultEnum res = sender.SendAnInvoiceToNav(InvoiceID);

                    if (res == QueueInvoiceResultEnum.Queued)
                    {
                        lock (lockobj)
                        {
                            succes += 1;
                        }
                    }
                });

            }

            Assert.AreEqual(max, succes);
        }
    }
}
