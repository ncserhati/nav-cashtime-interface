﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using System.Reflection;
using Ninject.Extensions.Xml;
using NLog;
using System.Configuration;

namespace nav_cashtime_interface
{
    public static class ExceptionExtension
    {
        public static string GetFullMessage(this Exception e)
        {
            return ConcatenateMessage(e);
        }

        private static string ConcatenateMessage(Exception e)
        {
            string Message = e.Message;

            if (e == null)
                return Message;
            else
            {
                if (e.InnerException != null)
                    return Message + ConcatenateMessage(e.InnerException);
                else
                    return Message;
            }
        }
    }

    class Program
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        static void Main(string[] args)
        {
            if (args.Length == 1)
            {
                if (args[0] == "test")
                {
                    string path = ConfigurationManager.AppSettings["InvoiceXmlDirectory"];

                    logger.Debug("Test file írási kísérlet");
                    try
                    {
                        System.IO.File.Create(path + "test.txt");
                        logger.Info("test.txt kiírása sikeres");
                    }
                    catch (Exception e) {
                        logger.Warn("test.txt kiírása SIKERTELEN : " + e.Message);
                    }
                }
                else
                {

                    int InvoiceID;

                    if (int.TryParse(args[0], out InvoiceID))
                    {

                        try
                        {
                            var kernel = new StandardKernel();

                            kernel.Load("NinjectBinding.xml");

                            var navExporter = kernel.Get<nav_cashtime_interface.infrastructure.INavInvoiceSender>();

                            nav_cashtime_interface.infrastructure.InvoiceSender sender = new infrastructure.InvoiceSender(navExporter);                            

                            sender.SendAnInvoiceToNav(InvoiceID);
                        }
                        catch (Exception e)
                        {
                            logger.Error("Ismeretlen hiba :" + e.GetFullMessage());
                        }
                    }
                    else
                    {
                        logger.Error("Helytelen Számlaazonosító :" + args[0]);
                    }
                }
            }
            else
            {
                logger.Error("Helytelen paraméterezés");
            }
        }
    }
}
