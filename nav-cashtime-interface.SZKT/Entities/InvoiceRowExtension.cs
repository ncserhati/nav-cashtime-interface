﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using nav_cashtime_interface.infrastructure;

namespace nav_cashtime_interface.SZKT.Entities
{
    public partial class InvoiceRow : IInvoiceRow
    {
        public  Neowell.NavInvoiceInterface.Domain.InvoiceApi.UnitOfMeasureType UnitOfMeasure {
            get {
                if (!InvoiceQuantityUnitID.HasValue && InvoiceQuantityUnit == null)
                    return Neowell.NavInvoiceInterface.Domain.InvoiceApi.UnitOfMeasureType.OWN;

                try
                {
                    Neowell.NavInvoiceInterface.Domain.InvoiceApi.UnitOfMeasureType conv;                    

                    Enum.TryParse<Neowell.NavInvoiceInterface.Domain.InvoiceApi.UnitOfMeasureType>(InvoiceQuantityUnit.NavCode, out conv);

                    return conv;
                }
                catch (Exception e) {
                    return Neowell.NavInvoiceInterface.Domain.InvoiceApi.UnitOfMeasureType.OWN;
                }
            }
        }

        public string InvoiceQuantityUnitName
        {
            get
            {
                if (InvoiceQuantityUnitID.HasValue && InvoiceQuantityUnit != null)
                    return InvoiceQuantityUnit.Description;
                else
                    return String.Empty;
            }
        }

        public Nullable<int> OrderIndex{ get  { return DocNo; } }

        public decimal? VatMultiplier
        {
            get
            {
                if (Vat1 != null)
                    return (decimal)Vat1.Multiplier;
                else
                    return (decimal?)null;
            }
        }

        decimal? IInvoiceRow.Gross
        {
            get
            {
                if (Gross.HasValue)
                    return (decimal)Gross;
                else
                    return (decimal?)null;
            }
        }

        decimal? IInvoiceRow.Net
        {
            get
            {
                if (Net.HasValue)
                    return (decimal)Net;
                else
                    return (decimal?)null;
            }
        }

        decimal? IInvoiceRow.Quantity
        {
            get
            {
                if (Quantity.HasValue)
                    return (decimal)Quantity;
                else
                    return (decimal?)null;
            }
        }

        decimal? IInvoiceRow.UnitPrice
        {
            get
            {
                if (UnitPrice.HasValue)
                    return (decimal)UnitPrice;
                else
                    return (decimal?)null;
            }
        }

        decimal? IInvoiceRow.Vat
        {
            get
            {
                if (Vat.HasValue)
                    return (decimal)Vat;
                else
                    return (decimal?)null;
            }
        }
    }
}
