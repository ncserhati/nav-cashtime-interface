﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using nav_cashtime_interface.infrastructure;
using nav_cashtime_interface.infrastructure.Object;
using Neowell.NavInvoiceInterface.Domain.InvoiceApi;

namespace nav_cashtime_interface.SZKT.Entities
{
    public partial class Invoice : IInvoiceHead
    {
        public Address SellerAddress { get; internal set; }

        public Address CustomerAddress { get; internal set; }

        public string SupplierName { get { return TextSeller;} }

        public string SupplierTaxNum{ get { return TextSellerTaxNum.Split(',')[0]; } }

        public string SellerBankAccountNum
        {
            get
            {
                if (String.IsNullOrEmpty(TextSellerAccNo))
                {
                    return String.Empty;
                }
                else
                {
                    //Ha számmal kezdődik
                    if (TextSellerAccNo[0] >= 48 && TextSellerAccNo[0] <= 57)
                    {
                        return TextSellerAccNo.Trim().Replace(" ", "");
                    }
                    else {
                        return TextSellerAccNo.Trim().Replace(" ", "").Replace("-","");
                    }
                }
            }
        }

        public string CustomerTaxNum{ get { return TextBuyerTaxNum; } }

        public string CustomerName { get { return TextBuyer; } }

        public string InvoiceNumber { get { return InvNo; } }

        public DateTime? InvoiceIssueDate
        {
            get
            {
                return InvDate;
            }
        }

        public PaymentMethodType Paymentmethod
        {
            get
            {
                return
                    InvoicePayCategoryID == 1 ? PaymentMethodType.CASH : 
                                InvoicePayCategoryID == 2 ? PaymentMethodType.TRANSFER : 
                                    InvoicePayCategoryID == 3 ? PaymentMethodType.CARD : 
                                        PaymentMethodType.OTHER;
            }
        }

        public InvoiceAppearanceType InvoiceAppearance
        {
            get { return ( IsEInvoice.HasValue && IsEInvoice.Value ) ? InvoiceAppearanceType.ELECTRONIC : InvoiceAppearanceType.PAPER;  }
        }

        public string CurrencyCode{get;set;}

        public Nullable<decimal> ExchangeRate { get { return DayCurrency; } }

        //Sztornózott számla adatai
        public ReferencInvoiceData CreditedInvoice { get; internal set; }

        public decimal? VatSum
        {
            get
            {
                return VATSum;
            }
        }

        //Helyesbített számla adatai
        public ReferencInvoiceData CorrectedInvoice { get; internal set; }

        public decimal? VatSumHUF {
            get {
                return BookedVat;
            }
        }
    }
}
