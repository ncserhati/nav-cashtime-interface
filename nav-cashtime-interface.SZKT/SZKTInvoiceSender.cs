﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using nav_cashtime_interface.infrastructure;

namespace nav_cashtime_interface.SZKT
{
    public class SZKTInvoiceSender : INavInvoiceSender
    {        
        public SZKTInvoiceSender() {           
        }
        /// <summary>
        /// E-számla esetén, akkor is generálódjon XML,ha nem kerül átadásra a NAV felé!
        /// </summary>
        /// <returns></returns>
        public bool? GetForceGenerateXMLForEInvoice()
        {
            using (Entities.CashTime_Entities context = new Entities.CashTime_Entities())
            {
                var param = context.SystemParameter.FirstOrDefault(x => x.ParameterName == "NAVExportForceGenerateXMLForEInvoice");

                if (param == null)
                    return (bool?)null;
                else
                {
                    bool vatLimit;
                    if (bool.TryParse(param.Value, out vatLimit))
                    {
                        return vatLimit;
                    }
                    else
                    {
                        return (bool?)null;
                    }
                }
            }
        }

        public decimal? GetVatLimit()
        {
            using (Entities.CashTime_Entities context = new Entities.CashTime_Entities())
            {
                var param = context.SystemParameter.FirstOrDefault(x => x.ParameterName == "NAVExportVATLimit");

                if (param == null)
                    return (int?)null;
                else
                {
                    decimal vatLimit;
                    if (decimal.TryParse(param.Value, out vatLimit))
                    {
                        return vatLimit;
                    }
                    else
                    {
                        return (decimal?)null;
                    }
                }
            }
        }

        public DateTime? GetEnabledFromDate()
        {
            using (Entities.CashTime_Entities context = new Entities.CashTime_Entities())
            {
                var param = context.SystemParameter.FirstOrDefault(x => x.ParameterName == "NAVExportEnabledFrom");

                if (param == null)
                    return (DateTime?)null;
                else
                {
                    DateTime EnabledFrom;
                    if (DateTime.TryParse(param.Value, out EnabledFrom))
                    {
                        return EnabledFrom;
                    }
                    else {
                        return (DateTime?)null;
                    }
                }
            }
        }

        public IInvoiceHead GetInvoiceByID(int InvoiceID)
        {
            Entities.CashTime_Entities context = new Entities.CashTime_Entities();

            Entities.Invoice inv =  context.Invoice.FirstOrDefault(x => x.InvoiceID == InvoiceID);

            if (inv != null)
            {
                string[] cName = inv.TextSellerZipCityCountry.Split(',');
                string country = cName[cName.Length-1].Replace(" ", "");
                Entities.Country sellerCountry = context.Country.FirstOrDefault(x => x.CountryName == country);

                inv.SellerAddress = new infrastructure.Object.Address();
                inv.SellerAddress.CountryCode = sellerCountry == null || String.IsNullOrEmpty(sellerCountry.Code) ? "HU" : sellerCountry.Code;
                inv.SellerAddress.City =  inv.TextSellerZipCityCountry.Substring(5, 8);
                inv.SellerAddress.ZipCode = inv.TextSellerZipCityCountry.Substring(0, 4);
                inv.SellerAddress.Details = inv.TextSellerStreet;

                Entities.Country custCountry = context.Country.FirstOrDefault(x => x.CountryName == inv.TextBuyerCountry);
                inv.CustomerAddress = new infrastructure.Object.Address();
                inv.CustomerAddress.CountryCode = custCountry == null || String.IsNullOrEmpty(custCountry.Code) ? "HU" : custCountry.Code;
                int temp;
                if (inv.TextBuyerZipCityCountry.Length >= 4 && int.TryParse(inv.TextBuyerZipCityCountry.Substring(0, 4), out temp) && !inv.TextBuyerZipCityCountry.Substring(0, 4).Contains(" "))
                {
                    inv.CustomerAddress.ZipCode = inv.TextBuyerZipCityCountry.Substring(0, 4);
                }
                else
                {
                    inv.CustomerAddress.ZipCode = "0000";
                }
                inv.CustomerAddress.City = inv.TextBuyerZipCityCountry.Length < 5 ? "" : inv.TextBuyerZipCityCountry.Substring(5, inv.TextBuyerZipCityCountry.Length - 5);
                inv.CustomerAddress.Details = String.IsNullOrEmpty(inv.TextBuyerStreet) ? String.Empty : inv.TextBuyerStreet;

                if (inv.CurrencyID.HasValue && inv.CurrencyID != 0)
                {
                    inv.CurrencyCode = context.Currency.First(c => c.CurrencyID == inv.CurrencyID).CurrencyCode;
                }

                if (context.InvoiceCredit.Any(c => c.CorrectInvoiceID == inv.InvoiceID))
                {
                    int creditedInvoiceId = context.InvoiceCredit.First(c => c.CorrectInvoiceID == inv.InvoiceID).InvoiceID;
                    var creditedInvoice = context.Invoice.First(i => i.InvoiceID == creditedInvoiceId);

                    inv.CreditedInvoice = new infrastructure.Object.ReferencInvoiceData();
                    inv.CreditedInvoice.InvoiceNumber = creditedInvoice.InvNo;
                    inv.CreditedInvoice.InvoiceIssueDate = creditedInvoice.InvDate.Value;
                }

                if (context.InvoiceCorrect.Any(x => x.CorrectInvoiceID == inv.InvoiceID))
                {
                    int correctedInvoiceID = context.InvoiceCorrect.First(c => c.CorrectInvoiceID == inv.InvoiceID).InvoiceID;
                    var correctedInvoiceInvoice = context.Invoice.First(i => i.InvoiceID == correctedInvoiceID);

                    inv.CorrectedInvoice = new infrastructure.Object.ReferencInvoiceData();
                    inv.CorrectedInvoice.InvoiceNumber = correctedInvoiceInvoice.InvNo;
                    inv.CorrectedInvoice.InvoiceIssueDate = correctedInvoiceInvoice.InvDate.Value;
                }
            }
            return inv;
        }

        public List<IInvoiceRow> GetInvoiceRowsByInvoiceID(int InvoiceID)
        {
            List<Entities.InvoiceRow> rows;

            using (Entities.CashTime_Entities context = new Entities.CashTime_Entities())
            {
                rows = context.InvoiceRow.Include("InvoiceQuantityUnit")
                                         .Include("Vat1")
                                         .Where(x => x.InvoiceID == InvoiceID).OrderBy(l => l.DocNo).ToList();
            }

            return rows.ToList<IInvoiceRow>();
        }

        public int GetUserId(int InvoiceID)
        {
            using (Entities.CashTime_Entities context = new Entities.CashTime_Entities())
            {
                var user = context.User.FirstOrDefault();

                if (user == null)
                    throw new Exception("Nincs User konfigurálva az adatbázisban");
                else
                    return user.UserId;
            }
        }

        public void UpdateInvoice(int InvoiceID, bool NavExportisSuccessful, string NAVXMLName)
        {
            using (Entities.CashTime_Entities entities = new Entities.CashTime_Entities())
            {
                var invoice = entities.Invoice.First(x => x.InvoiceID == InvoiceID);

                invoice.IsNAVExportSuccessful = NavExportisSuccessful;

                if (!String.IsNullOrEmpty(NAVXMLName))
                {
                    invoice.NAVXMLFileName = NAVXMLName;
                }

                entities.SaveChanges();
            }
        }        
    }
}
