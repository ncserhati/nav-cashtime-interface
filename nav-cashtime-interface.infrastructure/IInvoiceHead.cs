﻿using nav_cashtime_interface.infrastructure.Object;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nav_cashtime_interface.infrastructure
{
    public interface IInvoiceHead
    {
        int InvoiceID { get; }

        string SupplierTaxNum { get; }

        string SupplierName { get; }

        string SellerBankAccountNum { get; }

        Address SellerAddress { get; }

        string CustomerName { get; }

        string CustomerTaxNum { get; }

        Address CustomerAddress { get; }

        string InvoiceNumber { get; }

        Nullable<DateTime> InvoiceIssueDate { get; }

        Nullable<DateTime> DeliveryDate { get; }

        Nullable<DateTime> DueDate { get; }       

        Neowell.NavInvoiceInterface.Domain.InvoiceApi.PaymentMethodType Paymentmethod { get; }

        Neowell.NavInvoiceInterface.Domain.InvoiceApi.InvoiceAppearanceType InvoiceAppearance { get; }

        string CurrencyCode { get; }

        Nullable<decimal> ExchangeRate { get; }

        ReferencInvoiceData CreditedInvoice { get; }

        ReferencInvoiceData CorrectedInvoice {get;}

        Nullable<decimal> GrossSum { get;}

        Nullable<decimal> NetSum { get; }

        Nullable<decimal> VatSum { get; }

        Nullable<decimal> VatSumHUF { get; }
    }
}
