﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nav_cashtime_interface.infrastructure
{
    public interface INavInvoiceSender
    {
        IInvoiceHead GetInvoiceByID(int InvoiceID);

        Nullable<DateTime> GetEnabledFromDate();

        Nullable<decimal> GetVatLimit();

        Nullable<bool> GetForceGenerateXMLForEInvoice();

        int GetUserId(int InvoiceID);

        List<IInvoiceRow> GetInvoiceRowsByInvoiceID(int InvoiceID);

        void UpdateInvoice(int InvoiceID, bool NavExportisSuccessfull,string NAVXMLName);
    }
}
