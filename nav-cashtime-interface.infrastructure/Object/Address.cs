﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nav_cashtime_interface.infrastructure.Object
{
    public class Address
    {
        public string CountryCode {get;set;}
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string Details { get;  set; }
    }
}
