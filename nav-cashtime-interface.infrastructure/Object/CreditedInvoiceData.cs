﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nav_cashtime_interface.infrastructure.Object
{
    public class ReferencInvoiceData
    {
        public string InvoiceNumber { get; set; }

        public DateTime InvoiceIssueDate { get; set; }
    }
}
