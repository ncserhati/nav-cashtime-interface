﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nav_cashtime_interface.infrastructure
{
    public class Validators
    {
        /// <summary>
        /// Magyarországi adószám formátum validációs kifejezés! (00000000-0-00)
        /// </summary>
        public const string HUNTaxNumFormat = "^[0-9]{8}-[0-9]-[0-9]{2}$";

        public const string BanckAccNoFormat = "[0-9]{8}[-][0-9]{8}[-][0-9]{8}|[0-9]{8}[-][0-9]{8}|[A-Z]{2}[0-9]{2}[0-9A-Za-z]{11,30}";
    }
}
