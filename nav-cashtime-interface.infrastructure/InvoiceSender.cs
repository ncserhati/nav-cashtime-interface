﻿using nav_cashtime_interface.infrastructure.Extensions;
using Neowell.NavInvoiceInterface.Domain;
using Neowell.NavInvoiceInterface.Domain.Enums;
using Neowell.NavInvoiceInterface.Domain.InvoiceApi;
using Neowell.NavInvoiceInterface.Services;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace nav_cashtime_interface.infrastructure
{
    public static class ExceptionExtension
    {
        public static string GetFullMessage(this Exception e)
        {
            return ConcatenateMessage(e);
        }

        private static string ConcatenateMessage(Exception e)
        {
            string Message = String.Empty;

            if (e == null)
                return Message;
            else
            {
                if (e.InnerException != null)
                    return Message + ConcatenateMessage(e.InnerException);
                else
                    return Message;
            }
        }
    }

    public class InvoiceSender
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private INavInvoiceSender isender;

        public InvoiceSender(INavInvoiceSender _isender) {
            isender = _isender;
        }

        public QueueInvoiceResultEnum SendAnInvoiceToNav(int InvoiceID)
        {
            Nullable<DateTime> enableFromDate = isender.GetEnabledFromDate();
            Nullable<decimal> VATLimit = isender.GetVatLimit();
            Nullable<bool> NAVExportForceGenerateXMLForEInvoice = isender.GetForceGenerateXMLForEInvoice();

            if (!enableFromDate.HasValue)
                return QueueInvoiceResultEnum.UnexpectedException;

            if (!VATLimit.HasValue)
            {
                logger.Warn("Átadási Áfa határérték nincs beállítva!Az alapértelmezett értékkel fut generálás: 0 (minden számla átadásra kerül)!");
                VATLimit  = 0;
            }
            
            logger.Info("Számla feldolgozása ID alapján : " + InvoiceID);
            System.Threading.Thread.Sleep(500);
            try
            {
                int cnt = 1;
                IInvoiceHead head = null;
                while (head == null && cnt < 3) {
                    logger.Info(cnt+". kísérlet");
                    try
                    {
                        head = isender.GetInvoiceByID(InvoiceID);
                    }
                    catch (Exception e) {
                        logger.Error("Hiba a számlaadatok lekérdezése közben");
                        System.Threading.Thread.Sleep(500);
                        cnt++;
                    }
                }

            if (head == null)
            {
                logger.Error("Számla azonosítása sikertelen!");
            }
            else
            {
                logger.Info("Számla azonosítása sikeres!");

                QueueService inter = new QueueService();

                Invoice invoiceXml = new Invoice();
                InvoiceExchangeType exchangeType = new InvoiceExchangeType();
                invoiceXml.Item = exchangeType;

                exchangeType.invoiceHead = new InvoiceHeadType();                
                logger.Info("Számla adatok feldolgozása, NAV XML összállítása");

               
                    exchangeType.invoiceHead.supplierInfo = new SupplierInfoType();
                    if (!String.IsNullOrEmpty(head.SupplierTaxNum))
                    {
                        if (Regex.IsMatch(head.SupplierTaxNum, Validators.HUNTaxNumFormat))
                        {

                            exchangeType.invoiceHead.supplierInfo.supplierTaxNumber = new TaxNumberType();
                            exchangeType.invoiceHead.supplierInfo.supplierTaxNumber.taxpayerId = head.SupplierTaxNum.Substring(0, 8);
                            exchangeType.invoiceHead.supplierInfo.supplierTaxNumber.vatCode = head.SupplierTaxNum.Substring(9, 1);
                            exchangeType.invoiceHead.supplierInfo.supplierTaxNumber.countyCode = head.SupplierTaxNum.Substring(11, 2);
                        }
                    }

                    exchangeType.invoiceHead.supplierInfo.supplierName = head.SupplierName.ToSingleText(512);
                    if (!String.IsNullOrEmpty(head.SellerBankAccountNum))
                    {
                        if (Regex.IsMatch(head.SellerBankAccountNum, Validators.BanckAccNoFormat) && head.SellerBankAccountNum.Length<=34)
                        {
                            exchangeType.invoiceHead.supplierInfo.supplierBankAccountNumber = head.SellerBankAccountNum;
                        }
                    }

                    //Szállítói címadatok
                    exchangeType.invoiceHead.supplierInfo.supplierAddress = new AddressType();
                    SimpleAddressType address = new SimpleAddressType();
                    exchangeType.invoiceHead.supplierInfo.supplierAddress.Item = address;
                    address.countryCode = head.SellerAddress.CountryCode;
                    address.postalCode = head.SellerAddress.ZipCode;
                    address.city = head.SellerAddress.City.ToSingleText(255);
                    address.additionalAddressDetail = head.SellerAddress.Details.ToSingleText(255);

                    //Ügyfél adatok
                    exchangeType.invoiceHead.customerInfo = new CustomerInfoType();
                    exchangeType.invoiceHead.customerInfo.customerName = head.CustomerName.ToSingleText(512);
                    if (!String.IsNullOrEmpty(head.CustomerTaxNum))
                    {
                        if (Regex.IsMatch(head.CustomerTaxNum, Validators.HUNTaxNumFormat))
                        {

                            exchangeType.invoiceHead.customerInfo.customerTaxNumber = new TaxNumberType();
                            exchangeType.invoiceHead.customerInfo.customerTaxNumber.taxpayerId = head.CustomerTaxNum.Substring(0, 8);
                            exchangeType.invoiceHead.customerInfo.customerTaxNumber.vatCode = head.CustomerTaxNum.Substring(9, 1);
                            exchangeType.invoiceHead.customerInfo.customerTaxNumber.countyCode = head.CustomerTaxNum.Substring(11, 2);                                         
                        }
                        else
                        {
                            exchangeType.invoiceHead.customerInfo.thirdStateTaxId = head.CustomerTaxNum.ToSingleText(50);
                        }
                    }
                    
                    exchangeType.invoiceHead.customerInfo.customerAddress = new AddressType();
                    SimpleAddressType custaddress = new SimpleAddressType();
                    exchangeType.invoiceHead.customerInfo.customerAddress.Item = custaddress;
                    custaddress.countryCode = head.CustomerAddress.CountryCode;
                    custaddress.postalCode = head.CustomerAddress.ZipCode;
                    custaddress.city = head.CustomerAddress.City.ToSingleText(255);

                    custaddress.additionalAddressDetail = head.CustomerAddress.Details.ToSingleText(255);

                    exchangeType.invoiceHead.invoiceData = new InvoiceDataType();
                    exchangeType.invoiceHead.invoiceData.invoiceNumber = head.InvoiceNumber;
                    exchangeType.invoiceHead.invoiceData.invoiceCategory = InvoiceCategoryType.NORMAL;

                    if (head.InvoiceIssueDate.HasValue)
                    {
                        exchangeType.invoiceHead.invoiceData.invoiceIssueDate = head.InvoiceIssueDate.Value;
                        exchangeType.invoiceHead.invoiceData.invoiceIssueDateSpecified = true;
                    }

                    if (head.DeliveryDate.HasValue)
                    {
                        exchangeType.invoiceHead.invoiceData.invoiceDeliveryDate = head.DeliveryDate.Value;
                        exchangeType.invoiceHead.invoiceData.invoiceDeliveryDateSpecified = true;
                    }
                    if (head.DueDate.HasValue)
                    {
                        exchangeType.invoiceHead.invoiceData.paymentDate = head.DueDate.Value;
                        exchangeType.invoiceHead.invoiceData.paymentDateSpecified = true;
                    }

                    exchangeType.invoiceHead.invoiceData.paymentMethod = head.Paymentmethod;
                    exchangeType.invoiceHead.invoiceData.paymentMethodSpecified = true;

                    exchangeType.invoiceHead.invoiceData.invoiceAppearance = head.InvoiceAppearance;

                    if (!String.IsNullOrEmpty(head.CurrencyCode))
                    {
                        exchangeType.invoiceHead.invoiceData.currencyCode = head.CurrencyCode;

                        if (head.CurrencyCode != "HUF")
                        {
                            if (head.ExchangeRate.HasValue)
                            {
                                exchangeType.invoiceHead.invoiceData.exchangeRate = head.ExchangeRate.Value;
                            }
                        }
                        else {
                            exchangeType.invoiceHead.invoiceData.exchangeRate = 1;
                        }
                    }

                    if (head.CreditedInvoice != null)
                    {

                        exchangeType.invoiceReference = new InvoiceReferenceType();
                        exchangeType.invoiceReference.originalInvoiceNumber = head.CreditedInvoice.InvoiceNumber;
                        exchangeType.invoiceReference.modificationIssueDate = head.CreditedInvoice.InvoiceIssueDate;
                        exchangeType.invoiceReference.modificationTimestamp = head.CreditedInvoice.InvoiceIssueDate;

                        if (head.CreditedInvoice.InvoiceIssueDate < isender.GetEnabledFromDate().Value)
                        {
                            exchangeType.invoiceReference.modifyWithoutMaster = true;
                        }
                    }

                    if (head.CorrectedInvoice != null)
                    {
                        exchangeType.invoiceReference = new InvoiceReferenceType();
                        exchangeType.invoiceReference.originalInvoiceNumber = head.CorrectedInvoice.InvoiceNumber;
                        exchangeType.invoiceReference.modificationIssueDate = head.InvoiceIssueDate.Value;
                        exchangeType.invoiceReference.modificationTimestamp = head.InvoiceIssueDate.Value;

                        if (head.CorrectedInvoice.InvoiceIssueDate < isender.GetEnabledFromDate().Value)
                        {
                            exchangeType.invoiceReference.modifyWithoutMaster = true;
                        }
                    }

                    //Rows
                    List<IInvoiceRow> rows = isender.GetInvoiceRowsByInvoiceID(InvoiceID);
                    List<LineType> xmlLines = new List<LineType>();

                    int ii = 1;
                    foreach (var line in rows)
                    {
                        var xmlLine = new LineType();
                        xmlLine.lineNumber = ii.ToString();//line.OrderIndex == null ? ii.ToString() : line.OrderIndex.ToString();

                        xmlLine.lineExpressionIndicator = true;
                        xmlLine.lineExpressionIndicatorSpecified = true;

                        if (!string.IsNullOrEmpty(line.Description))
                        {
                            xmlLine.lineDescription = line.Description.ToSingleText(255);
                        }else
                        {
                            xmlLine.lineExpressionIndicator = false;
                        }

                        xmlLine.quantity = (line.Quantity ?? 1);
                        xmlLine.quantitySpecified = true;
                        //if (!String.IsNullOrEmpty(line.InvoiceQuantityUnitName))
                        //{
                        xmlLine.unitOfMeasure = line.UnitOfMeasure;//line.InvoiceQuantityUnitName.ToSingleText(50);
                        xmlLine.unitOfMeasureSpecified = true;

                        if (xmlLine.unitOfMeasure == UnitOfMeasureType.OWN) {
                            xmlLine.unitOfMeasureOwn = line.InvoiceQuantityUnitName.ToSingleText(50);
                        }
                        //}
                        if (line.UnitPrice.HasValue)
                        {
                            xmlLine.unitPrice = decimal.Round(((decimal)line.UnitPrice.Value), 2);
                            xmlLine.unitPriceSpecified = true;
                        }
                        else {
                            xmlLine.lineExpressionIndicator = false;
                        }
                        LineAmountsNormalType lineAmounts = new LineAmountsNormalType();
                        lineAmounts.lineNetAmount = decimal.Round((decimal)(line.Net ?? 0), 2);
                        lineAmounts.lineVatRate = new VatRateType();
                        lineAmounts.lineVatRate.ItemElementName = ItemChoiceType1.vatPercentage;
                        lineAmounts.lineVatRate.Item = (decimal)(line.VatMultiplier);
                        if (line.Vat.HasValue)
                        {
                            lineAmounts.lineVatAmount = decimal.Round((decimal)line.Vat.Value, 2);
                            lineAmounts.lineVatAmountSpecified = true;
                        }
                        if (line.BookedVat.HasValue)
                        {
                            lineAmounts.lineVatAmountHUF = decimal.Round((decimal)line.BookedVat.Value, 2);
                            lineAmounts.lineVatAmountHUFSpecified = true;
                        }
                        if (line.Gross.HasValue)
                        {
                            lineAmounts.lineGrossAmountNormal = decimal.Round((decimal)line.Gross.Value, 2);
                            lineAmounts.lineGrossAmountNormalSpecified = true;
                        }
                        xmlLine.Item = lineAmounts;

                        if (head.CreditedInvoice != null || head.CorrectedInvoice != null)
                        {
                            xmlLine.lineModificationReference = new LineModificationReferenceType();
                            xmlLine.lineModificationReference.lineOperation = (lineAmounts.lineNetAmount < 0 ? 
                                LineOperationType.MODIFY : 
                                LineOperationType.CREATE);

                            xmlLine.lineModificationReference.lineNumberReference = ii.ToString();//line.OrderIndex == null ? ii.ToString() : line.OrderIndex.ToString();
                        }

                        xmlLines.Add(xmlLine);
                        ii++;
                    }

                    exchangeType.invoiceLines = xmlLines.ToArray();

                    //Summary
                    exchangeType.invoiceSummary = new SummaryType();
                    exchangeType.invoiceSummary.invoiceGrossAmount = decimal.Round(head.GrossSum.Value, 2);
                    exchangeType.invoiceSummary.invoiceGrossAmountSpecified = true;
                    SummaryNormalType summery = new SummaryNormalType();
                    summery.invoiceNetAmount = head.NetSum.HasValue ? decimal.Round(head.NetSum.Value, 2) : 0;
                    summery.invoiceVatAmount = head.VatSum.HasValue ? decimal.Round(head.VatSum.Value, 2) : 0;
                    summery.invoiceVatAmountHUF = head.VatSumHUF.HasValue ? decimal.Round(head.VatSumHUF.Value,2) : 0;

                    SummaryByVatRateType summaryByVatRate = new SummaryByVatRateType();

                    List<SummaryByVatRateType> rate = 
                    rows.GroupBy(x => x.VatMultiplier).Select(su => new SummaryByVatRateType
                    {
                        vatRate = new VatRateType
                        {
                            ItemElementName = ItemChoiceType1.vatPercentage,
                            Item = su.Key
                        },
                        vatRateNetAmount = su.Sum(x => x.Net.HasValue ?  decimal.Round(x.Net.Value,2) : 0),
                        vatRateVatAmount = su.Sum(x => x.Vat.HasValue ? decimal.Round(x.Vat.Value,2) : 0),
                        vatRateVatAmountHUF = su.Sum(x => x.BookedVat.HasValue ? decimal.Round(x.BookedVat.Value, 2) : 0),
                        vatRateGrossAmount = su.Sum(x => x.Gross.HasValue ? decimal.Round(x.Gross.Value,2) : 0),

                        vatRateVatAmountHUFSpecified = true,
                        vatRateGrossAmountSpecified = true
                    }).ToList();

                    summery.summaryByVatRate = rate.ToArray();


                    exchangeType.invoiceSummary.Items = new object[] { summery };
                    

                    InvoiceTypeEnum type = head.CreditedInvoice != null ? InvoiceTypeEnum.Storno : (
                                            head.CorrectedInvoice != null ? InvoiceTypeEnum.Modify :  InvoiceTypeEnum.Create
                                           );

                    logger.Info("NAV Adatstruktúra sikeresen előkészítve");
                                       
                    if (head.VatSumHUF.HasValue && Math.Abs(head.VatSumHUF.Value) >= VATLimit)
                    {
                        logger.Info("Számla küldése a NAV-nak");                       

                        QueueInvoiceResult r =
                           inter.QueueInvoice(invoiceXml, type, isender.GetUserId(InvoiceID));                                                                                                                            
                       

                        if (r.Result == QueueInvoiceResultEnum.Queued)
                            logger.Info("Számla sikeresen elküldve a NAV-nak");
                        else
                            logger.Warn("Számla küldése sikertelen");


                        logger.Info("Cashtime Adatbázis frissítése");
                        isender.UpdateInvoice(InvoiceID, 
                            r.Result == QueueInvoiceResultEnum.Queued,
                            r.XmlFilePath);
                        logger.Info("Cashtime Adatbázis frissítve");
                        return r.Result;
                    }
                    else {
                        logger.Info(String.Format("A számla nem került átadásra a NAV - nak, mert az ÁFAtartlma({0} HUF) nem érte el a határértéket!", 
                            head.VatSumHUF.Value.ToString("N0")));

                        if (NAVExportForceGenerateXMLForEInvoice.HasValue && 
                            NAVExportForceGenerateXMLForEInvoice.Value && 
                            head.InvoiceAppearance == InvoiceAppearanceType.ELECTRONIC)
                        {
                            QueueInvoiceResult r =
                               inter.QueueInvoice(invoiceXml, type, isender.GetUserId(InvoiceID),true);

                            if (r.Result == QueueInvoiceResultEnum.Queued ||
                                r.Result == QueueInvoiceResultEnum.XmlGenerated)
                                logger.Info("Számla XML legenerálva!");
                            else
                                logger.Warn("XML generálás sikertelen");

                            logger.Info("Cashtime Adatbázis frissítése");
                            isender.UpdateInvoice(InvoiceID, 
                                r.Result == QueueInvoiceResultEnum.Queued,
                                r.XmlFilePath);
                            logger.Info("Cashtime Adatbázis frissítve");                            
                        }

                        return QueueInvoiceResultEnum.UnexpectedException;
                    }                
                }
            }
            catch (Exception exp)
            {
                logger.Info("Ismereretlen hiba! "  + (exp.Message + exp.InnerException!= null ? exp.InnerException.Message : ""));
            }

            return QueueInvoiceResultEnum.UnexpectedException;
        }
    }
}
