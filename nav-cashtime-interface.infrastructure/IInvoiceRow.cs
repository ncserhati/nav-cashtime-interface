﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nav_cashtime_interface.infrastructure
{
    public interface IInvoiceRow
    {
        Nullable<int> OrderIndex { get; }

        string Description { get; }

        Nullable<decimal> Quantity { get; }

        Nullable<decimal> UnitPrice { get; }

        string InvoiceQuantityUnitName { get;}

        Nullable<decimal> Net { get; }
        Nullable<decimal> Vat { get; }
        Nullable<decimal> BookedVat { get; }
        Nullable<decimal> BookedGross { get; }
        Nullable<decimal> BookedNet { get; }
        Nullable<decimal> Gross { get; }

        Nullable<decimal> VatMultiplier { get; }

        Neowell.NavInvoiceInterface.Domain.InvoiceApi.UnitOfMeasureType UnitOfMeasure { get; }
    }
}
