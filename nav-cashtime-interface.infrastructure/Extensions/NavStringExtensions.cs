﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nav_cashtime_interface.infrastructure.Extensions
{
    internal static class NavStringExtension
    {
        public static string ToSingleText(this string s, int maxLength)
        {
            //Speckó karakterek kivétele!
            string _test = s.Replace("\t", "").Replace("\n", "").Replace("\r", "");

            if (_test.Length <= maxLength)
                return _test;
            else
                return _test.Substring(0, maxLength);
        }
    }
}
