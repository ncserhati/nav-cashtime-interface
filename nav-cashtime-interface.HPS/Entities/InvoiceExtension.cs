﻿using nav_cashtime_interface.infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using nav_cashtime_interface.infrastructure.Object;
using Neowell.NavInvoiceInterface.Domain.InvoiceApi;

namespace nav_cashtime_interface.HPS.Entities
{
    public partial class Invoice : IInvoiceHead
    {
        //Sztornózott számla adatai
        public ReferencInvoiceData CreditedInvoice { get; internal set; }

        //Helyesbített számla adatai
        public ReferencInvoiceData CorrectedInvoice { get; internal set; }

        public string CurrencyCode { get; set; }

        public Address CustomerAddress { get; internal set; }

        public string CustomerName { get { return TextBuyer; } }

        public string CustomerTaxNum { get { return TextBuyerTaxNum; } }

        public Nullable<decimal> ExchangeRate { get { return DayCurrency; } }

        public InvoiceAppearanceType InvoiceAppearance
        {
            get { return (IsEInvoice.HasValue && IsEInvoice.Value) ? InvoiceAppearanceType.ELECTRONIC : InvoiceAppearanceType.PAPER; }
        }

        public DateTime? InvoiceIssueDate
        {
            get
            {
                return InvDate;
            }
        }

        public string InvoiceNumber { get { return InvNo; } }

        public PaymentMethodType Paymentmethod
        {
            get
            {
                return
                    InvoicePayCategoryID == 1 ? PaymentMethodType.CASH :
                                InvoicePayCategoryID == 2 ? PaymentMethodType.TRANSFER :
                                    InvoicePayCategoryID == 3 ? PaymentMethodType.CARD :
                                        PaymentMethodType.OTHER;
            }
        }

        public Address SellerAddress { get; internal set; }

        public string SellerBankAccountNum {
            get
            {
                if (String.IsNullOrEmpty(TextSellerAccNo))
                {
                    return String.Empty;
                }
                else
                {
                    return TextSellerAccNo.Trim().Replace(" ", "");
                }
            }
        }

        public string SupplierName { get { return TextSeller; } }

        public string SupplierTaxNum { get {
                if (TextSellerTaxNum.Contains(','))
                    return TextSellerTaxNum.Split(',')[0];
                else
                    return TextSellerTaxNum;
            }
        }

        public decimal? VatSum
        {
            get
            {
                return VATSum;
            }
        }

        public decimal? VatSumHUF
        { get{
                return BookedVat;
            }
        }
    }
}
