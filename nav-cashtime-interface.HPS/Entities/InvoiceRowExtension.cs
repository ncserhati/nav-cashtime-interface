﻿using nav_cashtime_interface.infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nav_cashtime_interface.HPS.Entities
{
    public partial class InvoiceRow : IInvoiceRow
    {
        public Neowell.NavInvoiceInterface.Domain.InvoiceApi.UnitOfMeasureType UnitOfMeasure
        {
            get
            {
                if (!InvoiceQuantityUnitID.HasValue && InvoiceQuantityUnit == null)
                    return Neowell.NavInvoiceInterface.Domain.InvoiceApi.UnitOfMeasureType.OWN;

                try
                {
                    Neowell.NavInvoiceInterface.Domain.InvoiceApi.UnitOfMeasureType conv;

                    Enum.TryParse<Neowell.NavInvoiceInterface.Domain.InvoiceApi.UnitOfMeasureType>(InvoiceQuantityUnit.NavCode, out conv);

                    return conv;
                }
                catch (Exception e)
                {
                    return Neowell.NavInvoiceInterface.Domain.InvoiceApi.UnitOfMeasureType.OWN;
                }
            }
        }

        public string InvoiceQuantityUnitName
        {
            get
            {
                if (InvoiceQuantityUnitID.HasValue && InvoiceQuantityUnit != null)
                    return InvoiceQuantityUnit.Description;
                else
                    return String.Empty;
            }
        }

        public Nullable<int> OrderIndex { get { return DocNo; } }

        public decimal? VatMultiplier
        {
            get
            {
                if (Vat1 != null)
                    return (decimal)Vat1.Multiplier;
                else
                    return (decimal?)null;
            }
        }
    }
}
