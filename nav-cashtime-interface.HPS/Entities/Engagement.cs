//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace nav_cashtime_interface.HPS.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class Engagement
    {
        public int EngagementID { get; set; }
        public string JobCode { get; set; }
        public Nullable<int> HoldingID { get; set; }
        public Nullable<int> HoldingBrandID { get; set; }
        public Nullable<int> ClientID { get; set; }
        public int DepartmentID { get; set; }
        public int TeamID { get; set; }
        public string EngagementName { get; set; }
        public int StateID { get; set; }
        public Nullable<int> SubStateID { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> FreezeDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public Nullable<System.DateTime> CloseDate { get; set; }
        public Nullable<System.DateTime> TenderEndDate { get; set; }
        public int ProjectManagerID { get; set; }
        public int ProjectDirectorID { get; set; }
        public Nullable<int> CurrencyID { get; set; }
        public Nullable<System.DateTime> BriefDate { get; set; }
        public string Comment { get; set; }
        public Nullable<int> ContactID { get; set; }
        public int EngagementTypeID { get; set; }
        public Nullable<int> CampaignID { get; set; }
        public Nullable<int> AdvGroupID { get; set; }
        public Nullable<bool> CulturalAffix { get; set; }
        public Nullable<decimal> AgentAffix { get; set; }
        public Nullable<bool> Partial { get; set; }
        public Nullable<System.DateTime> AcceptStatement { get; set; }
        public Nullable<int> ParentEngagementID { get; set; }
        public Nullable<int> EngagementCategoryID { get; set; }
        public Nullable<int> EngagementRenderTypeID { get; set; }
        public System.DateTime CreateDate { get; set; }
        public string ClientPO { get; set; }
        public string TaskingComment { get; set; }
        public Nullable<float> TimesheetMinHour { get; set; }
        public Nullable<double> PlannedBudget { get; set; }
        public Nullable<int> PlannedBudgetCurrencyID { get; set; }
        public Nullable<int> ClientContactID { get; set; }
        public Nullable<int> EventDays { get; set; }
        public Nullable<System.DateTime> DeliveryDate { get; set; }
        public Nullable<short> Month { get; set; }
        public Nullable<int> Year { get; set; }
        public Nullable<int> InnerContactID { get; set; }
        public Nullable<int> CreatorEmployeeID { get; set; }
        public Nullable<int> CostPlaceID { get; set; }
        public Nullable<int> DeliveryMethodID { get; set; }
        public Nullable<int> PaymentOffset { get; set; }
        public Nullable<int> PaymentOffsetType { get; set; }
        public Nullable<int> EngagementApprovingStateID { get; set; }
        public Nullable<double> BestBudget { get; set; }
        public Nullable<double> WorstBudget { get; set; }
        public Nullable<int> BestBudgetCurrencyID { get; set; }
        public Nullable<int> WorstBudgetCurrencyID { get; set; }
        public Nullable<bool> IsLocked { get; set; }
        public Nullable<int> LockerEmployeeID { get; set; }
        public Nullable<System.DateTime> LockDate { get; set; }
        public Nullable<bool> IsTimesheetNonChargeable { get; set; }
    
        public virtual Engagement Engagement1 { get; set; }
        public virtual Engagement Engagement2 { get; set; }
        public virtual Currency Currency { get; set; }
        public virtual Currency Currency1 { get; set; }
    }
}
