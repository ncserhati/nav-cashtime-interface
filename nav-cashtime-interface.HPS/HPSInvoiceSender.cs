﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using nav_cashtime_interface.infrastructure;

namespace nav_cashtime_interface.HPS
{
    public class HPSInvoiceSender : nav_cashtime_interface.infrastructure.INavInvoiceSender
    {
        public DateTime? GetEnabledFromDate()
        {
            using (Entities.CashTime_Entities context = new Entities.CashTime_Entities())
            {
                var param = context.SystemParameter.FirstOrDefault(x => x.ParameterName == "NAVExportEnabledFrom");

                if (param == null)
                    return (DateTime?)null;
                else
                {
                    DateTime EnabledFrom;
                    if (DateTime.TryParse(param.Value, out EnabledFrom))
                    {
                        return EnabledFrom;
                    }
                    else
                    {
                        return (DateTime?)null;
                    }
                }
            }
        }

        public bool? GetForceGenerateXMLForEInvoice()
        {
            return false;
        }

        public IInvoiceHead GetInvoiceByID(int InvoiceID)
        {
            Entities.CashTime_Entities context = new Entities.CashTime_Entities();

            Entities.Invoice inv = context.Invoice.FirstOrDefault(x => x.InvoiceID == InvoiceID);

            if (inv != null)
            {
                string[] cName = inv.TextSellerZipCityCountry.Split(',');
                string country = cName[cName.Length-1].Replace(" ", "");
                Entities.Country sellerCountry = context.Country.FirstOrDefault(x => x.CountryName == country);

                inv.SellerAddress = new infrastructure.Object.Address();
                inv.SellerAddress.CountryCode = sellerCountry == null || String.IsNullOrEmpty(sellerCountry.Code) ? "HU" : sellerCountry.Code;
                inv.SellerAddress.City = String.IsNullOrEmpty(inv.TextSellerZipCityCountry) ? ""  : inv.TextSellerZipCityCountry.Substring(5, 8);
                inv.SellerAddress.ZipCode = String.IsNullOrEmpty(inv.TextSellerZipCityCountry) ? "":  inv.TextSellerZipCityCountry.Substring(0, 4);
                inv.SellerAddress.Details = inv.TextSellerStreet;

                Entities.Country custCountry = context.Country.FirstOrDefault(x => x.CountryName == inv.TextBuyerCountry);

                inv.CustomerAddress = new infrastructure.Object.Address();
                inv.CustomerAddress.CountryCode = custCountry == null || String.IsNullOrEmpty(custCountry.Code) ? "HU" : custCountry.Code;
                int temp;
                if (!String.IsNullOrEmpty(inv.TextBuyerZipCityCountry) && inv.TextBuyerZipCityCountry.Length >= 4 && int.TryParse(inv.TextBuyerZipCityCountry.Substring(0, 4), out temp) && !inv.TextBuyerZipCityCountry.Substring(0, 4).Contains(" "))
                {
                    inv.CustomerAddress.ZipCode = inv.TextBuyerZipCityCountry.Substring(0, 4);
                }
                else
                {
                    inv.CustomerAddress.ZipCode = "0000";
                }
                inv.CustomerAddress.City = String.IsNullOrEmpty(inv.TextBuyerZipCityCountry) || inv.TextBuyerZipCityCountry.Length < 5 ? "" : inv.TextBuyerZipCityCountry.Substring(5, inv.TextBuyerZipCityCountry.Length - 5);
                inv.CustomerAddress.Details = inv.TextBuyerStreet;

                if (inv.CurrencyID.HasValue && inv.CurrencyID != 0)
                {
                    inv.CurrencyCode = context.Currency.First(c => c.CurrencyID == inv.CurrencyID).CurrencyCode;
                }

                if (context.InvoiceCredit.Any(c => c.CorrectInvoiceID == inv.InvoiceID))
                {
                    int creditedInvoiceId = context.InvoiceCredit.First(c => c.CorrectInvoiceID == inv.InvoiceID).InvoiceID;
                    var creditedInvoice = context.Invoice.FirstOrDefault(i => i.InvoiceID == creditedInvoiceId);

                    if (creditedInvoice == null)
                        throw new Exception("Invalid DB configuration the Credited Invoice is not exists");

                    inv.CreditedInvoice = new infrastructure.Object.ReferencInvoiceData();
                    inv.CreditedInvoice.InvoiceNumber = creditedInvoice.InvNo;
                    inv.CreditedInvoice.InvoiceIssueDate = creditedInvoice.InvDate.Value;
                }

                if (context.InvoiceCorrect.Any(x => x.InvoiceCorrectID == inv.InvoiceID))
                {
                    int correctedInvoiceID = context.InvoiceCorrect.First(c => c.CorrectInvoiceID == inv.InvoiceID).InvoiceID;
                    var correctedInvoiceInvoice = context.Invoice.First(i => i.InvoiceID == correctedInvoiceID);

                    inv.CreditedInvoice = new infrastructure.Object.ReferencInvoiceData();
                    inv.CreditedInvoice.InvoiceNumber = correctedInvoiceInvoice.InvNo;
                    inv.CreditedInvoice.InvoiceIssueDate = correctedInvoiceInvoice.InvDate.Value;
                }
            }
            return inv;
        }

        public List<IInvoiceRow> GetInvoiceRowsByInvoiceID(int InvoiceID)
        {
            List<Entities.InvoiceRow> rows;

            using (Entities.CashTime_Entities context = new Entities.CashTime_Entities())
            {
                rows = context.InvoiceRow.Include("InvoiceQuantityUnit")
                                         .Include("Vat1")
                                         .Where(x => x.InvoiceID == InvoiceID).OrderBy(l => l.DocNo).ToList();
            }

            return rows.ToList<IInvoiceRow>();
        }

        public int GetUserId(int InvoiceID)
        {
            using (Entities.CashTime_Entities entities = new Entities.CashTime_Entities())
            {
                var EngagementID = entities.Invoice.First(x => x.InvoiceID == InvoiceID).EngagementID;

                var DepartmentID = entities.Engagement.First(x => x.EngagementID == EngagementID).DepartmentID;

                var conf = entities.DepartmentToNavUser.FirstOrDefault(x => x.DepartmentId == DepartmentID);

                if (conf == null)
                    throw new Exception("NAV user nincs bálllítva a kövekező Department-hez:" + DepartmentID.ToString());
                else
                    return conf.NavUserId;
            }
        }

        public decimal? GetVatLimit()
        {
            using (Entities.CashTime_Entities context = new Entities.CashTime_Entities())
            {
                var param = context.SystemParameter.FirstOrDefault(x => x.ParameterName == "NAVExportVATLimit");

                if (param == null)
                    return (int?)null;
                else
                {
                    decimal vatLimit;
                    if (decimal.TryParse(param.Value, out vatLimit))
                    {
                        return vatLimit;
                    }
                    else
                    {
                        return (decimal?)null;
                    }
                }
            }
        }

        public void UpdateInvoice(int InvoiceID, bool NavExportisSuccessful, string NAVXMLName)
        {
            using (Entities.CashTime_Entities entities = new Entities.CashTime_Entities())
            {
                var invoice = entities.Invoice.First(x => x.InvoiceID == InvoiceID);

                invoice.IsNAVExportSuccessful = NavExportisSuccessful;

                entities.SaveChanges();
            }
        }
    }
}
